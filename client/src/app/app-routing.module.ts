import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/security/login.component';
import { RegisterComponent } from './components/security/register.component';
import { ProfileComponent } from './components/security/profile.component';
import { ForgotPasswordComponent } from './components/security/forgot-password.component';

import { MainComponent } from './components/main/main.component';
import { AuthcodeComponent } from './components/security/authcode.component';

import { AuthGuard } from "./shared/auth.guard";
const routes: Routes = [
  { path: "", component: LoginComponent },
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },
  { path: "authcode", component: AuthcodeComponent},
  { path: "forgot-password", component: ForgotPasswordComponent},
  { path: "main", component: MainComponent, canActivate: [AuthGuard] },
  { path: "profile/:id", component: ProfileComponent, canActivate: [AuthGuard] },
  { path: "", redirectTo: "/", pathMatch: "full" },
  { path: "**", redirectTo: "/", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
