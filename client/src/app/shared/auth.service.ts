import { Injectable, NgZone } from '@angular/core';
import { User } from './user';
import { Observable, throwError, Subject } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  endpoint: string = environment.api_url;
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  currentUser = {};
  public getLoggedInName = new Subject(); 

  constructor(
    private http: HttpClient,
    public router: Router,
    private _snackBar : MatSnackBar,
    private zone: NgZone
  ) {
  }

  // Sign-up
  signUp(user: User): Observable<any> {
    let api = `${this.endpoint}/register-user`;
    return this.http.post(api, user)
      .pipe(
        retry(1),
        catchError(this.handleError.bind(this))
      )
  }

  // Sign-in
  signIn(user: User) {
    return this.http.post<any>(`${this.endpoint}/signin`, user)
      .pipe(
        retry(1),
        catchError(this.handleError.bind(this))
      )
      .subscribe((res: any) => {
        localStorage.setItem('access_token', res.token);
        this.getLoggedInName.next(res.token);
        this.getUserProfile(res._id).subscribe((res) => {
          this.currentUser = res;
          console.log(this.currentUser);
          localStorage.setItem('user_id', res.msg._id);
          this._snackBar.open("Sigin successfully", "close", {
            duration: 2000,
          });
          this.router.navigate(['/profile/' + res.msg._id]);
        })
      })
  }

  getToken() {
    return localStorage.getItem('access_token');
  }

  get isLoggedIn(): boolean {
    let authToken = localStorage.getItem('access_token');
    return (authToken !== null) ? true : false;
  }

  doLogout() {
    let removeToken = localStorage.removeItem('access_token');
  }

  // User profile
  getUserProfile(id): Observable<any> {
    console.log(id)
    let api = `${this.endpoint}/user-profile/${id}`;
    return this.http.get(api, { headers: this.headers }).pipe(
      map((res: Response) => {
        return res || {}
      }),
      catchError(this.handleError)
    )
  }

  setupAuth(data) {
    return this.http.post(`${environment.tfa_api_url}/setup`, data, { observe: 'response' })
  }

  getAuth() {
    return this.http.get(`${environment.tfa_api_url}/setup`, { observe: 'response' });
  }

  deleteAuth() {
    return this.http.delete(`${environment.tfa_api_url}/setup`, { observe: 'response' });
  }

  verifyAuth(token: any) {
    return this.http.post(`${environment.tfa_api_url}/verify`, { token }, { observe: 'response' });
  }

  // Error 
  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = error.error.message;
      console.log("msg client -> "+ msg);
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
       console.log("msg server side -> "+ msg);
    }
    //window.alert(msg);
    this.zone.run(() => {
      this._snackBar.open(msg, "close", {
        duration: 2000,
      });
    });
    return throwError(msg);
  }
}