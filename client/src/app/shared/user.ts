export class User {
    _id: String;
    firstName?: String;
    lastName?: String;
    authcode?: string;
    email: String;
    password: String;
}