import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './shared/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'client';
  public user_id : any;

  constructor(private router: Router, 
    private authSvc: AuthService,
    private snackBar: MatSnackBar
    ){
    this.authSvc.getLoggedInName.subscribe(userId => this.user_id = userId);
  }

  ngOnInit(){
    // empty
  }

  showUserProfile(){
    this.user_id = localStorage.getItem('user_id');
    console.log(this.user_id);
    this.router.navigate([`/profile/${this.user_id}`]);
  }

  signIn(){
    this.router.navigate(['/']);
  }

  signUp(){
    this.router.navigate(['/register']);
  }

  logout(){
    console.log("Logout>>");
    this.authSvc.doLogout();
    this.user_id = null;
    this.snackBar.open("Logout !", "close", {
      duration: 2000,
    });
    this.router.navigate(['/login']);
  }
}
