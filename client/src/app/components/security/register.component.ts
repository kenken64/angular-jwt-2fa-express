import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,Validators } from "@angular/forms";
import { AuthService } from '../../shared/auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MustMatch } from '../../validators/must-match';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  signupForm: FormGroup;
  PASSWORD_PATTERN = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{12,}$/;

  constructor(public fb: FormBuilder,
    public authService: AuthService,
    public router: Router,
    public _snackBar: MatSnackBar) {
      this.signupForm = fb.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.pattern(this.PASSWORD_PATTERN)]],
        confirmPassword: ['', [Validators.required, Validators.pattern(this.PASSWORD_PATTERN)]],
        firstName: ['', [Validators.required, Validators.minLength(3)]],
        lastName: ['', [Validators.required, Validators.minLength(3)]], 
      },{validators: MustMatch('password', 'confirmPassword')})
  }

  ngOnInit() {
  }

  get fc() { return this.signupForm.controls; }

  register(){
    console.log("register....");
    this.authService.signUp(this.signupForm.value)
      .subscribe((result)=>{
        console.log(result);
        this.signupForm.reset();
        this._snackBar.open("Thank you for signing up!", "close", {
          duration: 2000,
        });
        this.router.navigate(['login']);
      });
  }
}
