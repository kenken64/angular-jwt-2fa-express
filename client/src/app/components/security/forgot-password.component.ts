import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { AuthService } from '../../shared/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  resetPasswordForm: FormGroup;

  constructor(public fb: FormBuilder,
    public authService: AuthService,
    public router: Router) { 
      this.resetPasswordForm = new FormGroup({
        'email': new FormControl('', [
          Validators.required,
          Validators.email
        ])
      });
  }

  get fc() { return this.resetPasswordForm.controls; }

  ngOnInit() {

  }

  reset(){

  }

}
