import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/auth.service';
import { ThemePalette } from '@angular/material/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user: any = null;
  tfa: any = null;
  authcode: string = "";
  errorMessage: string = null;
  color: ThemePalette = 'accent';
  checked = false;
  disabled = false;

  constructor(private authSvc : AuthService) {
    
  }

  ngOnInit() {
    let userId = localStorage.getItem('user_id');
    console.log("userid: " +userId);
    this.authSvc.getUserProfile(userId).subscribe((result)=>{
      this.user = result;
      console.log(result);
      this.tfa = result.msg;
      console.log(this.tfa);
      this.checked = result.msg.tfaEnabled;
      delete this.user.msg.password;
      this.tfa.tempSecret =  this.user.msg.secret;
    })
  }

  toggle2FA(event){
    console.log(this.checked);
    if(this.checked){
      let data = {
        id: this.user.msg._id,
        email: this.user.msg.email,
        tfaEnabled: this.checked
      }
      this.authSvc.setupAuth(data).subscribe((data) => {
        const result = data.body
        if (data['status'] === 200) {
          console.log(result);
          this.tfa = result;
          console.log(this.tfa);
        }
      });
    }else{
      this.tfa = null;
    }
    
  }

  confirm(){

  }
}
