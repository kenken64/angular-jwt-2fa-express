export const environment = {
  production: true,
  api_url: 'http://localhost:3000/api',
  tfa_api_url: 'http://localhost:3000/tfa'
};
