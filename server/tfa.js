const express = require('express');
const speakeasy = require('speakeasy');
const QRCode = require('qrcode');
const router = express.Router();
const userSchema = require("./user");

router.post('/tfa/setup', (req, res) => {
    console.log("ddddd");
    console.log(`DEBUG: Received TFA setup request`);
    let id = req.body.id;
    let email = req.body.email;
    const secret = speakeasy.generateSecret({
        length: 10,
        name: email,
        issuer: 'NUSS ISS - Test'
    });
    let url = speakeasy.otpauthURL({
        secret: secret.base32,
        label: email,
        issuer: 'NUSS ISS - Test',
        encoding: 'base32'
    });
    QRCode.toDataURL(url, (err, dataURL) => {
        userSchema.findByIdAndUpdate(id, {
            $set: {
                secret: secret.base32,
                dataURL: dataURL,
                tfaEnabled: req.body.tfaEnabled
            }
        }, (error, data) => {
            if (error) {
                console.log(error);
                return res.status(500).json(error);
            } else {
                console.log('User successfully updated!')
                console.log("tempSecret > " + secret.base32);
                return res.status(200).json({
                    message: 'TFA Auth needs to be verified',
                    tempSecret: secret.base32,
                    dataURL,
                    tfaURL: secret.otpauth_url
                });
            }
        });
        
    });
});

router.get('/tfa/setup', (req, res) => {
    console.log(`DEBUG: Received FETCH TFA request`);
    res.status(200).json({});
});

router.delete('/tfa/setup', (req, res) => {
    console.log(`DEBUG: Received DELETE TFA request`);
    res.send({
        "status": 200,
        "message": "success"
    });
});

router.post('/tfa/verify', (req, res) => {
    console.log(`DEBUG: Received TFA Verify request`);
    let id = req.body.id;
    let authToken = req.body.token;
    userSchema.findById(id, (error, data) => {
        if (error) {
            return next(error);
        } else {
            let isVerified = speakeasy.totp.verify({
                secret: data.secret,
                encoding: 'base32',
                token: authToken,
                window: 5
            });

            if (isVerified) {
                console.log(`DEBUG: TFA is verified to be enabled`);
                return res.send({
                    "status": 200,
                    "message": "Two-factor Auth is enabled successfully"
                });
            }

            console.log(`ERROR: TFA is verified to be wrong`);

            return res.send({
                "status": 403,
                "message": "Invalid Auth Code, verification failed. Please verify the system Date and Time"
            });
        }
    })
});

module.exports = router;